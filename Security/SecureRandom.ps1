<#
.AUTHOR
Can Althaus

.SYNOPSIS
Generates cryptographically secure random data

.DESCRIPTION
Generates cryptographically secure random data. Provides int, uint, long, ulong, double and String.

.EXAMPLE
$allowedChars = "abcdefghijklmnopoqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
$secureRandom = [SecureRandom]::new()
$secureRandom.GetRandomInt32()
$secureRandom.GetRandomString(12, $allowedChars)
###  or  ###
[SecureRandom]::new().GetRandomUInt64()
[SecureRandom]::new().GetRandomDouble()
#>
class SecureRandom {

    hidden [System.Byte[]] GetBytes($length) {
        $cryptoProvider = [System.Security.Cryptography.RNGCryptoServiceProvider]::new()
        $byteArray = [System.Byte[]]::CreateInstance([System.Byte],$length)
        $cryptoProvider.GetBytes($byteArray)
        return $byteArray
    }

    [int] GetRandomInt32() {
        $byteArray = $this.GetBytes(4)
        $rand = [System.BitConverter]::ToInt32($byteArray, 0);
        return $rand
    }

    [uint32] GetRandomUInt32() {
        $byteArray = $this.GetBytes(4)
        $rand = [System.BitConverter]::ToUInt32($byteArray, 0);
        return $rand
    }

    [double] GetRandomDouble() {
        $byteArray = $this.GetBytes(8)
        $rand = [System.BitConverter]::ToDouble($byteArray, 0);
        return $rand
    }

     [long] GetRandomInt64() {
        $byteArray = $this.GetBytes(8)
        $rand = [System.BitConverter]::ToInt64($byteArray, 0);
        return $rand
    }

    [uint64] GetRandomUInt64() {
        $byteArray = $this.GetBytes(8)
        $rand = [System.BitConverter]::ToUInt64($byteArray, 0);
        return $rand
    }

    [String] GetRandomString($length, $allowedChars) {
        if($length.GetType().Name -ne "Int32") {
            throw '$length must be of Type Int32!'
        }
        if($length -le 0) {
            throw '$length must be a positive number!'
        }
        $type = $allowedChars.GetType().Name
        if(-Not ($type -eq "String" -or $type -eq "Object[]" -or $type -eq "Char[]")) {
            throw '$allowedChars must be a String, an Object[] containing characters, or a Char[]!'
        }

        $stringBuilder = [System.Text.StringBuilder]::new()
        for($i = 0; $i -lt $length; $i++) {
            $nextCharIndex = $this.GetRandomUInt32() % $allowedChars.length
            $nextChar = $allowedChars[$nextCharIndex]
            $stringBuilder.Append($nextChar)
        }
        return $stringBuilder.ToString()
    }
}

